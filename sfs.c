/*
 * This is where the implementation of the SFS driver starts
 * Authors:
 *  Adrian Albu <a.albu@vu.nl>
 */

#define FUSE_USE_VERSION 26

#include <errno.h>
#include <fuse.h>
#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <assert.h>
#include <time.h>

#include "sfs.h"
#include "diskio.h"

static const char default_img[] = "test.img";

/* Options passed from commandline arguments */
struct options
{
    const char *img;
    int background;
    int verbose;
    int show_help;
    int show_fuse_help;
} options;

#define log(fmt, ...)                         \
    do                                        \
    {                                         \
        if (options.verbose)                  \
            printf(" # " fmt, ##__VA_ARGS__); \
    } while (0)

/* libfuse2 leaks, so let's shush LeakSanitizer if we are using Asan. */
const char *__asan_default_options() { return "detect_leaks=0"; }

/*
 * Retrieve information about a file or directory.
 * You should populate fields of `stbuf` with appropriate information if the
 * file exists and is accessible, or return an error otherwise.
 *
 * For directories, you should at least set st_mode (with S_IFDIR) and st_nlink.
 * For files, you should at least set st_mode (with S_IFREG), st_nlink and
 * st_size.
 *
 * Return 0 on success, < 0 on error.
 */

static int sfs_getattr(const char *path,
                       struct stat *st)
{
    //  int res = -ENOSYS;

    log("my getattribute for path -> %s\n", path);

    memset(st, 0, sizeof(struct stat));
    /* Set owner to user/group who mounted the image */
    st->st_uid = getuid();
    st->st_gid = getgid();
    /* Last accessed/modified just now */
    st->st_atime = time(NULL);
    st->st_mtime = time(NULL);

    if (strcmp(path, "/") == 0)
    {
        st->st_mode = S_IFDIR | 0755;
        st->st_nlink = 2;
        return 0;
    }
    else
    {
        //file is beyind root dir
        char arguments[120][58];
        int nr_of_args = 0;
        int current_argument_char_index = 0;

        for (int char_index = 0; char_index < 1024; char_index++)
        {
            if (current_argument_char_index > 57)
            {
                return -ENAMETOOLONG;
            }

            if (path[char_index] == '/' && char_index > 0)
            {
                arguments[nr_of_args][current_argument_char_index] = '\0';
                nr_of_args++;
                current_argument_char_index = 0;
            }
            else if (path[char_index] == 0x00)
            {
                arguments[nr_of_args][current_argument_char_index] = '\0';
                break;
            }
            else if (path[char_index] != '/')
            {
                arguments[nr_of_args][current_argument_char_index] = path[char_index];
                current_argument_char_index++;
            }
        }

        struct sfs_entry root_dir[SFS_ROOTDIR_NENTRIES];
        disk_read(root_dir, SFS_ROOTDIR_SIZE, SFS_ROOTDIR_OFF);
        int current_argument = 0;

        for (unsigned file_index_in_root = 0; file_index_in_root < SFS_ROOTDIR_NENTRIES; file_index_in_root++)
        {
            if (strcmp(root_dir[file_index_in_root].filename, arguments[current_argument]) == 0)
            {
                if (root_dir[file_index_in_root].size & SFS_DIRECTORY)
                {
                    if (nr_of_args == 0)
                    {
                        st->st_mode = S_IFDIR | 0755;
                        st->st_nlink = 2;
                        return 0;
                    }
                }
                else if (root_dir[file_index_in_root].first_block != SFS_BLOCKIDX_EMPTY)
                {
                    /* code */
                    st->st_mode = S_IFREG | 0755;
                    st->st_nlink = 1;
                    st->st_size = root_dir[file_index_in_root].size & SFS_SIZEMASK;
                    return 0;
                }

                current_argument++; //jump to next argument
                struct sfs_entry entries_subdir[SFS_DIR_NENTRIES];
                disk_read(entries_subdir, SFS_DIR_SIZE, SFS_DATA_OFF + (root_dir[file_index_in_root].first_block - 1) * 512);
                int entry_found = 0;

                while (entry_found == 0 && current_argument <= nr_of_args)
                {
                    for (unsigned index = 0; index < SFS_DIR_NENTRIES; index++)
                    {
                        if (strcmp(entries_subdir[index].filename, arguments[current_argument]) == 0)
                        {
                            if (current_argument == nr_of_args)
                            {
                                entry_found = 1;
                                if (entries_subdir[index].size & SFS_DIRECTORY)
                                {
                                    st->st_mode = S_IFDIR | 0755;
                                    st->st_nlink = 2;
                                    return 0;
                                }
                                else if (entries_subdir[index].first_block != SFS_BLOCKIDX_EMPTY)
                                {
                                    st->st_mode = S_IFREG | 0755;
                                    st->st_nlink = 1;
                                    st->st_size = entries_subdir[index].size & SFS_SIZEMASK;
                                    return 0;
                                }
                            }
                            else
                            {
                                disk_read(entries_subdir, SFS_DIR_SIZE, SFS_DATA_OFF + (entries_subdir[index].first_block - 1) * 512);
                            }
                            break;
                        }
                    }
                    current_argument++;
                }
            }
        }
    }

    return -ENOENT;
}

/*
 * Return directory contents for `path`. This function should simply fill the
 * filenames - any additional information (e.g., whether something is a file or
 * directory) is later retrieved through getattr calls.
 * Use the function `filler` to add an entry to the directory. Use it like:
 *  filler(buf, <dirname>, NULL, 0);
 * Return 0 on success, < 0 on error.
 */
static int sfs_readdir(const char *path, //####################################################################################################################
                       void *buf,
                       fuse_fill_dir_t filler,
                       off_t offset,
                       struct fuse_file_info *fi)
{
    (void)offset, (void)fi;
    log("READDIR path  %s\n", path);

    //log(" \n");
    filler(buf, ".", NULL, 0);
    filler(buf, "..", NULL, 0);

    char arguments[120][58];
    (void)arguments;
    int nr_of_args = 0;
    int current_argument_char_index = 0;

    for (int char_index = 0; char_index < 1024; char_index++)
    {
        if (path[char_index] == '/' && char_index > 0)
        {
            arguments[nr_of_args][current_argument_char_index] = '\0';
            nr_of_args++;
            current_argument_char_index = 0;
        }
        else if (path[char_index] == 0x00)
        {
            arguments[nr_of_args][current_argument_char_index] = '\0';
            break;
        }
        else if (path[char_index] != '/')
        {
            arguments[nr_of_args][current_argument_char_index] = path[char_index];
            current_argument_char_index++;
        }
    }

    struct sfs_entry rootdir_d[SFS_ROOTDIR_NENTRIES];

    disk_read(rootdir_d, SFS_ROOTDIR_SIZE, SFS_ROOTDIR_OFF);

    if (strcmp(path, "/") != 0)
    {
        char arguments[120][58];
        int nr_of_args = 0;
        int current_argument_char_index = 0;

        for (int char_index = 0; char_index < 1024; char_index++)
        {
            if (current_argument_char_index > 57)
            {
                return -ENAMETOOLONG;
            }

            if (path[char_index] == '/' && char_index > 0)
            {
                arguments[nr_of_args][current_argument_char_index] = '\0';
                nr_of_args++;
                current_argument_char_index = 0;
            }
            else if (path[char_index] == 0x00)
            {
                arguments[nr_of_args][current_argument_char_index] = '\0';
                break;
            }
            else if (path[char_index] != '/')
            {
                arguments[nr_of_args][current_argument_char_index] = path[char_index];
                current_argument_char_index++;
            }
        }

        struct sfs_entry rootdir[SFS_ROOTDIR_NENTRIES]; //we must start here
        disk_read(rootdir, SFS_ROOTDIR_SIZE, SFS_ROOTDIR_OFF);
        int current_argument = 0;
        for (unsigned file_index_from_root = 0; file_index_from_root < SFS_ROOTDIR_NENTRIES; file_index_from_root++)
        {
            if (strcmp(rootdir[file_index_from_root].filename, arguments[current_argument]) == 0) //current argument is a directory inside the root folder
            {
                if (rootdir[file_index_from_root].size & SFS_DIRECTORY) //current argument is a directory inside the root folder
                {
                    struct sfs_entry parent_dir[SFS_DIR_NENTRIES];                                                             // This directory is now parent
                    disk_read(parent_dir, SFS_DIR_SIZE, SFS_DATA_OFF + (rootdir[file_index_from_root].first_block - 1) * 512); // we list the contents of this directory
                                                                                                                               // //log("Current argument %i and nr of args %i \n", current_argument, nr_of_args);
                    int dir_from_root = 0;
                    if (current_argument == nr_of_args) //if this is the only dir in arguments, we stop
                    {
                        dir_from_root = 1;
                    }
                    while (current_argument < nr_of_args && dir_from_root == 0) // there are more than 1 arguments
                    {
                        current_argument++;
                        for (unsigned file_index_from_parent = 0; file_index_from_parent < SFS_DIR_NENTRIES; file_index_from_parent++) //lopp thoguh contents of parent directory
                        {
                            if (strcmp(parent_dir[file_index_from_parent].filename, arguments[current_argument]) == 0) //we have now found the next directory in the entries of the the current parent
                            {
                                disk_read(parent_dir, SFS_DIR_SIZE, SFS_DATA_OFF + (parent_dir[file_index_from_parent].first_block - 1) * 512);
                                break;
                            }
                        }
                    }

                    for (unsigned k = 0; k < SFS_DIR_NENTRIES; k++)
                    {
                        if (strlen(parent_dir[k].filename) > 0)
                        {
                            filler(buf, parent_dir[k].filename, NULL, 0);
                        }
                    }
                    return 0;
                }
            }
        }
    }
    else
    {
        for (unsigned i = 0; i < SFS_ROOTDIR_NENTRIES; i++)
        {
            if (strlen(rootdir_d[i].filename) > 0)
            {
                filler(buf, rootdir_d[i].filename, NULL, 0);
            }
        }
    }
    return 0;
}

/*
 * Read contents of `path` into `buf` for  up to `size` bytes.
 * Note that `size` may be bigger than the file actually is.
 * Reading should start at offset `offset`; the OS will generally read your file
 * in chunks of 4K byte.
 * Returns the number of bytes read (writting into `buf`), or < 0 on error.
 */
static int sfs_read(const char *path,
                    char *buf,
                    size_t size,
                    off_t offset,
                    struct fuse_file_info *fi)
{
    (void)fi;
    unsigned bytes_read = 0;

    log("sfs_read path: %s size=%zu offset=%ld given_buf_size=%lu \n", path, size, offset, sizeof(buf));

    struct sfs_entry rootdir[SFS_ROOTDIR_NENTRIES];
    disk_read(rootdir, SFS_ROOTDIR_SIZE, SFS_ROOTDIR_OFF);
    void *entry = malloc(sizeof(blockidx_t));
    unsigned current_buf_index = 0;

    for (unsigned i = 0; i < SFS_ROOTDIR_NENTRIES; i++)
    {
        if (strcmp(rootdir[i].filename, &path[1]) == 0)
        {
            //read the first block of data
            blockidx_t firstBlock_idx = rootdir[i].first_block;

            int skips = 0;

            while (offset >= 512)
            {
                skips++;
                offset = offset - 512;
            }

            char *tmp = malloc(512);
            disk_read(tmp, SFS_BLOCK_SIZE, SFS_DATA_OFF + (firstBlock_idx - 1) * SFS_BLOCK_SIZE);

            //skip offset bytes from first block
            if (skips > 0)
            {
                //skip first block
                skips = skips - 1;
            }
            else
            {
                /* code */
                if (offset >= 0)
                {
                }
                else
                {
                    offset = 0;
                }
                for (int i = offset; i < 512; i++)
                {
                    if (current_buf_index < size)
                    {
                        buf[current_buf_index] = tmp[i];
                        current_buf_index++;
                        bytes_read++;
                    }
                }
                offset = 0;
            }

            disk_read(entry, sizeof(blockidx_t), SFS_BLOCKTBL_OFF + (firstBlock_idx - 1) * sizeof(blockidx_t));
            blockidx_t next_block_idx = *(blockidx_t *)entry;

            char *current_buf = malloc(512);

            while (next_block_idx != SFS_BLOCKIDX_END)
            {
                disk_read(current_buf, SFS_BLOCK_SIZE, SFS_DATA_OFF + (next_block_idx - 1) * SFS_BLOCK_SIZE); //read the next block of data

                if (skips > 0)
                {
                    //skip
                    skips = skips - 1;
                }
                else
                {
                    /* code */
                    if (offset >= 0)
                    {
                    }
                    else
                    {
                        offset = 0;
                    }

                    for (int i = offset; i < 512; i++)
                    {
                        if (current_buf_index < size)
                        {
                            buf[current_buf_index] = current_buf[i];
                            current_buf_index++;
                            bytes_read++;
                        }
                    }
                    offset = 0;
                }

                disk_read(entry, sizeof(blockidx_t), SFS_BLOCKTBL_OFF + (next_block_idx - 1) * sizeof(blockidx_t));
                next_block_idx = *(blockidx_t *)entry;
            }

            return size;
        }
    }
    //file is not in root directory
    char arguments[120][58];
    int nr_of_args = 0;
    int current_argument_char_index = 0;

    for (int char_index = 0; char_index < 1024; char_index++)
    {
        if (path[char_index] == '/' && char_index > 0)
        {
            arguments[nr_of_args][current_argument_char_index] = '\0';
            nr_of_args++;
            current_argument_char_index = 0;
        }
        else if (path[char_index] == 0x00)
        {
            arguments[nr_of_args][current_argument_char_index] = '\0';
            break;
        }
        else if (path[char_index] != '/')
        {
            arguments[nr_of_args][current_argument_char_index] = path[char_index];
            current_argument_char_index++;
        }
    }

    struct sfs_entry root_dir[SFS_ROOTDIR_NENTRIES];
    disk_read(root_dir, SFS_ROOTDIR_SIZE, SFS_ROOTDIR_OFF);
    int current_argument = 0;

    for (unsigned file_index_in_root = 0; file_index_in_root < SFS_ROOTDIR_NENTRIES; file_index_in_root++)
    {
        if (strcmp(root_dir[file_index_in_root].filename, arguments[current_argument]) == 0)
        {
            if (root_dir[file_index_in_root].size & SFS_DIRECTORY)
            {
                if (nr_of_args == 0)
                {
                    return 0;
                }
            }
            current_argument++; //jump to next argument
            struct sfs_entry entries_subdir[SFS_DIR_NENTRIES];
            disk_read(entries_subdir, SFS_DIR_SIZE, SFS_DATA_OFF + (root_dir[file_index_in_root].first_block - 1) * 512);
            int entry_found = 0;

            while (entry_found == 0 && current_argument <= nr_of_args)
            {
                for (unsigned index = 0; index < SFS_DIR_NENTRIES; index++)
                {
                    if (strcmp(entries_subdir[index].filename, arguments[current_argument]) == 0)
                    {
                        if (current_argument == nr_of_args)
                        {
                            entry_found = 1;
                            if (entries_subdir[index].size & SFS_DIRECTORY)
                            {
                                // if it is directory
                            }
                            else if (entries_subdir[index].first_block != SFS_BLOCKIDX_EMPTY)
                            {
                                //it is a file
                                blockidx_t firstBlock_idx = entries_subdir[index].first_block;
                                char *tmp = malloc(512);
                                disk_read(tmp, SFS_BLOCK_SIZE, SFS_DATA_OFF + (firstBlock_idx - 1) * SFS_BLOCK_SIZE);
                                int current_buf_index = 0;

                                for (int i = 0; i < 512; i++)
                                {
                                    buf[current_buf_index] = tmp[i];
                                    current_buf_index++;
                                }
                                bytes_read += 512;

                                disk_read(entry, sizeof(blockidx_t), SFS_BLOCKTBL_OFF + (firstBlock_idx - 1) * sizeof(blockidx_t));
                                blockidx_t next_block_idx = *(blockidx_t *)entry;

                                char *current_buf = malloc(512);

                                while (next_block_idx != SFS_BLOCKIDX_END)
                                {
                                    disk_read(current_buf, SFS_BLOCK_SIZE, SFS_DATA_OFF + (next_block_idx - 1) * SFS_BLOCK_SIZE); //read the next block of data

                                    for (int i = 0; i < 512; i++)
                                    {
                                        buf[current_buf_index] = current_buf[i];
                                        current_buf_index++;
                                    }

                                    bytes_read += SFS_BLOCK_SIZE;

                                    disk_read(entry, sizeof(blockidx_t), SFS_BLOCKTBL_OFF + (next_block_idx - 1) * sizeof(blockidx_t));
                                    next_block_idx = *(blockidx_t *)entry;
                                }
                                return bytes_read;
                            }
                            return 0;
                        }
                        else
                        {
                            disk_read(entries_subdir, SFS_DIR_SIZE, SFS_DATA_OFF + (entries_subdir[index].first_block - 1) * 512);
                        }
                        break;
                    }
                }
                current_argument++;
            }
        }
    }
    return 0;
}

/*
 * Create directory at `path`.
 * Returns 0 on success, < 0 on error.
 */
static int sfs_mkdir(const char *path,
                     mode_t mode)
{
    log("mkdir %s mode=%o\n", path, mode);

    return -ENOSYS;
}

/*
 * Remove directory at `path`.
 * Directories may only be removed if they are empty, otherwise this function
 * should return -ENOTEMPTY.
 * Returns 0 on success, < 0 on error.
 */
static int sfs_rmdir(const char *path)
{
    log("rmdir %s\n", path);

    return -ENOSYS;
}

/*
 * Remove file at `path`.
 * Can not be used to remove directories.
 * Returns 0 on success, < 0 on error.
 */
static int sfs_unlink(const char *path)
{
    struct sfs_entry rootdir[SFS_ROOTDIR_NENTRIES];
    disk_read(rootdir, SFS_ROOTDIR_SIZE, SFS_ROOTDIR_OFF);
    void *entry = malloc(sizeof(blockidx_t));
    blockidx_t unsused = SFS_BLOCKIDX_EMPTY;

    for (unsigned i = 0; i < SFS_ROOTDIR_NENTRIES; i++)
    {
        if (strcmp(rootdir[i].filename, &path[1]) == 0)
        {
            rootdir[i].size = 0;
            rootdir[i].filename[0] = 0x00;

            blockidx_t firstBlock_idx = rootdir[i].first_block;
            rootdir[i].first_block = SFS_BLOCKIDX_EMPTY;
            disk_write(rootdir, SFS_ROOTDIR_SIZE, SFS_ROOTDIR_OFF);

            disk_read(entry, sizeof(blockidx_t), SFS_BLOCKTBL_OFF + (firstBlock_idx - 1) * sizeof(blockidx_t));
            blockidx_t next_block_idx = *(blockidx_t *)entry;
            disk_write((void *)&unsused, sizeof(blockidx_t), SFS_BLOCKTBL_OFF + (firstBlock_idx - 1) * sizeof(blockidx_t));

            while (next_block_idx != SFS_BLOCKIDX_END && next_block_idx != SFS_BLOCKIDX_EMPTY)
            {
                disk_read(entry, sizeof(blockidx_t), SFS_BLOCKTBL_OFF + (next_block_idx - 1) * sizeof(blockidx_t));
                disk_write((void *)&unsused, sizeof(blockidx_t), SFS_BLOCKTBL_OFF + (next_block_idx - 1) * sizeof(blockidx_t));
                next_block_idx = *(blockidx_t *)entry;
            }
            disk_write((void *)&unsused, sizeof(blockidx_t), SFS_BLOCKTBL_OFF + (next_block_idx - 1) * sizeof(blockidx_t));
            return 0;
        }
    }
    char arguments[120][58];
    int nr_of_args = 0;
    int current_argument_char_index = 0;

    for (int char_index = 0; char_index < 1024; char_index++)
    {
        if (path[char_index] == '/' && char_index > 0)
        {
            arguments[nr_of_args][current_argument_char_index] = '\0';
            nr_of_args++;
            current_argument_char_index = 0;
        }
        else if (path[char_index] == 0x00)
        {
            arguments[nr_of_args][current_argument_char_index] = '\0';
            break;
        }
        else if (path[char_index] != '/')
        {
            arguments[nr_of_args][current_argument_char_index] = path[char_index];
            current_argument_char_index++;
        }
    }

    struct sfs_entry root_dir[SFS_ROOTDIR_NENTRIES];
    disk_read(root_dir, SFS_ROOTDIR_SIZE, SFS_ROOTDIR_OFF);
    int current_argument = 0;

    for (unsigned file_index_in_root = 0; file_index_in_root < SFS_ROOTDIR_NENTRIES; file_index_in_root++)
    {
        if (strcmp(root_dir[file_index_in_root].filename, arguments[current_argument]) == 0)
        {
            if (root_dir[file_index_in_root].size & SFS_DIRECTORY)
            {
                if (nr_of_args == 0)
                {
                    return 0;
                }
            }

            current_argument++; //jump to next argument
            struct sfs_entry entries_subdir[SFS_DIR_NENTRIES];

            disk_read(entries_subdir, SFS_DIR_SIZE, SFS_DATA_OFF + (root_dir[file_index_in_root].first_block - 1) * 512);
            unsigned last_block_table_offset = SFS_DATA_OFF + (root_dir[file_index_in_root].first_block - 1) * 512;
            int entry_found = 0;

            while (entry_found == 0 && current_argument <= nr_of_args)
            {
                for (unsigned index = 0; index < SFS_DIR_NENTRIES; index++)
                {
                    if (strlen(entries_subdir[index].filename) > 0 && strcmp(entries_subdir[index].filename, arguments[current_argument]) == 0)
                    {
                        if (current_argument == nr_of_args)
                        {
                            entry_found = 1;
                            if (entries_subdir[index].size & SFS_DIRECTORY)
                            {
                                //log("Entry is a folder \n");
                            }
                            else if (entries_subdir[index].first_block != SFS_BLOCKIDX_EMPTY)
                            {
                                entries_subdir[index].size = 0;
                                entries_subdir[index].filename[0] = '\0';

                                blockidx_t firstBlock_idx = entries_subdir[index].first_block;
                                entries_subdir[index].first_block = SFS_BLOCKIDX_EMPTY;

                                disk_write(entries_subdir, SFS_DIR_SIZE, last_block_table_offset); //????

                                disk_read(entry, sizeof(blockidx_t), SFS_BLOCKTBL_OFF + (firstBlock_idx - 1) * sizeof(blockidx_t));
                                blockidx_t next_block_idx = *(blockidx_t *)entry;
                                disk_write(&unsused, sizeof(blockidx_t), SFS_BLOCKTBL_OFF + (firstBlock_idx - 1) * sizeof(blockidx_t));

                                while (next_block_idx != SFS_BLOCKIDX_END && next_block_idx != SFS_BLOCKIDX_EMPTY)
                                {
                                    disk_read(entry, sizeof(blockidx_t), SFS_BLOCKTBL_OFF + (next_block_idx - 1) * sizeof(blockidx_t));
                                    disk_write(&unsused, sizeof(blockidx_t), SFS_BLOCKTBL_OFF + (next_block_idx - 1) * sizeof(blockidx_t));
                                    next_block_idx = *(blockidx_t *)entry;
                                }
                                disk_write(&unsused, sizeof(blockidx_t), SFS_BLOCKTBL_OFF + (next_block_idx - 1) * sizeof(blockidx_t));
                            }
                            return 0;
                        }
                        else
                        {
                            last_block_table_offset = SFS_DATA_OFF + (entries_subdir[index].first_block - 1) * 512;
                            disk_read(entries_subdir, SFS_DIR_SIZE, SFS_DATA_OFF + (entries_subdir[index].first_block - 1) * 512);
                        }
                        break;
                    }
                }
                current_argument++;
            }
        }
    }
    return -ENOENT;
}

/*
 * Create an empty file at `path`.
 * Returns 0 on success, < 0 on error.
 */
static int sfs_create(const char *path,
                      mode_t mode,
                      struct fuse_file_info *fi)
{
    (void)fi;
    log("create %s mode=%o\n", path, mode);
    char arguments[120][58];
    int nr_of_args = 0;
    int current_argument_char_index = 0;

    for (int char_index = 0; char_index < 1024; char_index++)
    {
        if (current_argument_char_index > 57)
        {
            return -1;
        }

        if (path[char_index] == '/' && char_index > 0)
        {
            arguments[nr_of_args][current_argument_char_index] = '\0';
            nr_of_args++;
            current_argument_char_index = 0;
        }
        else if (path[char_index] == 0x00)
        {
            arguments[nr_of_args][current_argument_char_index] = '\0';
            break;
        }
        else if (path[char_index] != '/')
        {
            arguments[nr_of_args][current_argument_char_index] = path[char_index];
            current_argument_char_index++;
        }
    }

    struct sfs_entry rootdir[SFS_ROOTDIR_NENTRIES];
    disk_read(rootdir, SFS_ROOTDIR_SIZE, SFS_ROOTDIR_OFF);

    if (nr_of_args == 0)
    {
        for (unsigned i = 0; i < SFS_ROOTDIR_NENTRIES; i++)
        {
            if (rootdir[i].first_block == SFS_BLOCKIDX_EMPTY)
            {
                int filename_chari = 0;
                while (arguments[0][filename_chari] != '\0')
                {
                    rootdir[i].filename[filename_chari] = arguments[0][filename_chari];
                    filename_chari++;
                }
                rootdir[i].filename[filename_chari] = '\0';
                rootdir[i].size = 0;

                void *entry = malloc(sizeof(blockidx_t));
                blockidx_t block_in_table;
                unsigned blck_index = sizeof(blockidx_t);
                do
                {
                    disk_read(entry, sizeof(blockidx_t), SFS_BLOCKTBL_OFF + blck_index);
                    block_in_table = *(blockidx_t *)entry;
                    blck_index += sizeof(blockidx_t);

                } while (block_in_table != SFS_BLOCKIDX_EMPTY);

                rootdir[i].first_block = SFS_BLOCKIDX_END;
                disk_write(rootdir, SFS_ROOTDIR_SIZE, SFS_ROOTDIR_OFF);
                return 0;
            }
        }
    }
    else
    {
        int current_argument = 0;
        for (unsigned r_index = 0; r_index < SFS_ROOTDIR_NENTRIES; r_index++)
        {
            if (strcmp(rootdir[r_index].filename, arguments[0]) == 0)
            {
                current_argument++;
                if (rootdir[r_index].size & SFS_DIRECTORY)
                {
                    struct sfs_entry entries_subdir[SFS_DIR_NENTRIES];
                    disk_read(entries_subdir, SFS_DIR_SIZE, SFS_DATA_OFF + (rootdir[r_index].first_block - 1) * 512);
                    unsigned sub_dir_location = 0;

                    while (current_argument < nr_of_args)
                    {
                        for (unsigned index = 0; index < SFS_DIR_NENTRIES; index++)
                        {
                            if (strcmp(entries_subdir[index].filename, arguments[current_argument]) == 0)
                            {
                                current_argument++;
                                sub_dir_location = SFS_DATA_OFF + (entries_subdir[index].first_block - 1) * 512;
                                disk_read(entries_subdir, SFS_DIR_SIZE, SFS_DATA_OFF + (entries_subdir[index].first_block - 1) * 512);
                            }
                        }
                    }

                    for (unsigned i = 0; i < SFS_DIR_NENTRIES; i++)
                    {
                        if (entries_subdir[i].first_block == SFS_BLOCKIDX_EMPTY)
                        {
                            int filename_chari = 0;
                            while (arguments[current_argument][filename_chari] != '\0')
                            {
                                entries_subdir[i].filename[filename_chari] = arguments[current_argument][filename_chari];
                                filename_chari++;
                            }
                            entries_subdir[i].filename[filename_chari] = '\0';

                            entries_subdir[i].size = 0;
                            void *entry = malloc(sizeof(blockidx_t));
                            blockidx_t block_in_table;
                            unsigned blck_index = sizeof(blockidx_t);
                            do
                            {
                                disk_read(entry, sizeof(blockidx_t), SFS_BLOCKTBL_OFF + blck_index);
                                block_in_table = *(blockidx_t *)entry;
                                blck_index += sizeof(blockidx_t);

                            } while (block_in_table != SFS_BLOCKIDX_EMPTY);

                            entries_subdir[i].first_block = SFS_BLOCKIDX_END;
                            disk_write(entries_subdir, SFS_DIR_SIZE, sub_dir_location);
                            return 0;
                        }
                    }
                }
            }
        }
    }

    return -ENOSYS;
}

/*
 * Shrink or grow the file at `path` to `size` bytes.
 * Excess bytes are thrown away, whereas any bytes added in the process should
 * be nil (\0).
 * Returns 0 on success, < 0 on error.
 */
static int sfs_truncate(const char *path, off_t size)
{
    log("truncate %s size=%ld\n", path, size);

    return -ENOSYS;
}

/*
 * Write contents of `buf` (of `size` bytes) to the file at `path`.
 * The file is grown if nessecary, and any bytes already present are overwritten
 * (whereas any other data is left intact). The `offset` argument specifies how
 * many bytes should be skipped in the file, after which `size` bytes from
 * buffer are written.
 * This means that the new file size will be max(old_size, offset + size).
 * Returns the number of bytes written, or < 0 on error.
 */
static int sfs_write(const char *path,
                     const char *buf,
                     size_t size,
                     off_t offset,
                     struct fuse_file_info *fi)
{

    (void)path;
    (void)buf;
    (void)size;
    (void)fi;
    (void)offset;
    return -ENOSYS;
}

/*
 * Move/rename the file at `path` to `newpath`.
 * Returns 0 on succes, < 0 on error.
 */
static int sfs_rename(const char *path,
                      const char *newpath)
{
    /* Implementing this function is optional, and not worth any points. */
    log("rename %s %s\n", path, newpath);

    return -ENOSYS;
}

static const struct fuse_operations sfs_oper = {
    .getattr = sfs_getattr,
    .readdir = sfs_readdir,
    .read = sfs_read,
    .mkdir = sfs_mkdir,
    .rmdir = sfs_rmdir,
    .unlink = sfs_unlink,
    .create = sfs_create,
    .truncate = sfs_truncate,
    .write = sfs_write,
    .rename = sfs_rename,
};

#define OPTION(t, p)                      \
    {                                     \
        t, offsetof(struct options, p), 1 \
    }
#define LOPTION(s, l, p) \
    OPTION(s, p),        \
        OPTION(l, p)
static const struct fuse_opt option_spec[] = {
    LOPTION("-i %s", "--img=%s", img),
    LOPTION("-b", "--background", background),
    LOPTION("-v", "--verbose", verbose),
    LOPTION("-h", "--help", show_help),
    OPTION("--fuse-help", show_fuse_help),
    FUSE_OPT_END};

static void show_help(const char *progname)
{
    printf("usage: %s mountpoint [options]\n\n", progname);
    printf("By default this FUSE runs in the foreground, and will unmount on\n"
           "exit. If something goes wrong and FUSE does not exit cleanly, use\n"
           "the following command to unmount your mountpoint:\n"
           "  $ fusermount -u <mountpoint>\n\n");
    printf("common options (use --fuse-help for all options):\n"
           "    -i, --img=FILE      filename of SFS image to mount\n"
           "                        (default: \"%s\")\n"
           "    -b, --background    run fuse in background\n"
           "    -v, --verbose       print debug information\n"
           "    -h, --help          show this summarized help\n"
           "        --fuse-help     show full FUSE help\n"
           "\n",
           default_img);
}

int main(int argc, char **argv)
{
    struct fuse_args args = FUSE_ARGS_INIT(argc, argv);

    options.img = strdup(default_img);

    fuse_opt_parse(&args, &options, option_spec, NULL);

    if (options.show_help)
    {
        show_help(argv[0]);
        return 0;
    }

    if (options.show_fuse_help)
    {
        assert(fuse_opt_add_arg(&args, "--help") == 0);
        args.argv[0][0] = '\0';
    }

    if (!options.background)
        assert(fuse_opt_add_arg(&args, "-f") == 0);

    disk_open_image(options.img);

    return fuse_main(args.argc, args.argv, &sfs_oper, NULL);
}