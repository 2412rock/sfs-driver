========================
SFS Driver
========================


This is the implementation of my own driver for the filesystem: SFS. This simple filesystem is
loosely based on the FAT filesystem, and supports (sub)directories and files of arbitrary size. It does have
some severe limitations, such as a maximum filesystem size of 8 MB, only a limited number of items per
directory, and no metadata such as file permissions.

Thie driver allows that files and directories can be navigated, read, created, removed and modified. 
The SFS filesystem is contained in an image. The driver will read and write to this image as if it is a disk.
The driver is implemented in userspace, through a framework called FUSE. With FUSE, drivers are
implemented as a normal (userspace) binary, and are called by the FUSE kernel driver when a user
interacts with the files. By using FUSE, the driver can mount the SFS filesystem
under a directory, allowing to navigate into it using some file browser, or using the terminal

Layout of SFS
The diagram below shows a conceptual overview of an SFS partition/image (not to scale). All
datatypes and information about the layout here are also provided in sfs.h in the framework

+------------------------+
| Magic numbers |
| 16 bytes |
+------------------------+
| Root directory entries |
| 4096 bytes |
+------------------------+
| |
| Block table |
| 32 Kbyte |
| |
+------------------------+
| |
| |
| Data area |
| remainder (~8 Mbyte) |
| |
| |
+------------------------+
Each SFS partition starts with 16 bytes of magic numbers. These identify the partition as SFS, and if
these numbers are not found by the driver or any of the SFS tools, they abort to prevent
corrupting data.
After the magic numbers follows the contents of the **root directory** of the
SFS partition. The entries in the root directory point either to files or
subdirectories.

Following the root directory is the **block table**. Each file and subdirectory
keeps its data in **disk blocks** of 512 byte. Files consisting of multiple
blocks do not necessarily have consecutive data, and this block table describes,
for each block on disk, what the *next* block is. For the last block in a chain
(i.e., the last block in a file) a special marker is used: ``SFS_BLOCKIDX_END``
(``0xfffe``). Additionally, a value of ``SFS_BLOCKIDX_EMPTY`` (``0``) in this
table indicates a block is unused, and can thus be allocated when creating
a file or directory. Because of these markers, block indices use a special
encoding, where the first block on the disk (in the data area) has block index
1 (instead of 0). The block table has space for ``0x4000`` entries.

The data area spans the rest of the disk, and consists of ``0x4000`` blocks of
512 byte each, resulting in 8 MB of data.


Directory entry format
----------------------

Each directory consists of a fixed number of entries, each representing a file
or subdirectory (or unused space). These are described by the following layout:

.. code:: c

   typedef uint16_t blockidx_t;

   struct sfs_entry {
      char filename[58];
      blockidx_t first_block;
      uint32_t size;
   } __attribute__((__packed__));



Creating disk images with mkfs
------------------------------

The ``mkfs.sfs`` binary produces a valid SFS image (that you can mount) with any
contents you specify. For all supported options, see ``.mkfs.sfs --help``.

To produce an image with this README, and empty directory foo, and an empty file
bar/baz, you can run the following command::

   $ ./mkfs.sfs test.img /README:README.rst /foo/ /bar/baz
   Creating fresh SFS filesystem
   Creating file '/README' from host file 'README.rst'
   Creating empty file '/bar/baz'



